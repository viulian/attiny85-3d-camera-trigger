# ATtiny85 3D Camera Trigger

This is the source code for an Atmel ATtiny85 microcontroller that allows simultaneous trigger of two digital cameras by using a single push button.

For more details see [http://hex.ro/wp/blog/3d-diy-camera-rig-based-on-sony-dsc-w800](http://hex.ro/wp/blog/3d-diy-camera-rig-based-on-sony-dsc-w800/)

# Requires

[DebounceInput](https://github.com/PaulMurrayCbr/DebounceInput) software library - to handle debouncing the pushbutton.