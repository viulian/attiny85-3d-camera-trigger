/** 
 * Copyright (c) viulian, 2016. Released into the public domain
 * under the unlicense http://unlicense.org .
 */

#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <DebounceInput.h>

/* Make sure that NONE of the pins have the PULLUPs activated, otherwise the current draw is too much. 
 * Edge detection requires the system clock to be running (but since MCU is sleeping, then it can't be used)
 */

// --- state machine

typedef enum
{
  STATE_SLEEP = 0,
  STATE_FOCUS,
  STATE_TIMER,
  STATE_SHUTTER
} my_state_t;

unsigned long lastFocusTime; // used to detect if the next state thould be the timer or the shutter.
DebouncedInput button; 

my_state_t state = STATE_SLEEP;

// -- pins

const int buttonPIN = 1; // PB1
const int statusLED = 2;
const int focusPIN  = 4; // PB4
const int shuttPIN  = 3; // PB3 

// -- setup

void setup() {
  pinMode(statusLED, OUTPUT);
  pinMode(focusPIN, OUTPUT);
  pinMode(shuttPIN, OUTPUT);
  
  pinMode(buttonPIN, INPUT); // NO PULLUP (use external to reduce current draw during sleep)
} // setup

void sleep() {
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // replaces above statement
  cli();                                  // make sure we don't get interrupted before we sleep
  sleep_enable();                         // Sets the Sleep Enable bit in the MCUCR Register (SE BIT)
  GIMSK |= _BV(PCIE);                     // Enable Pin Change Interrupts
  PCMSK |= _BV(PCINT1);                   // Use PB1 as interrupt pin // attachInterrupt
  ADCSRA &= ~_BV(ADEN);                   // ADC off
  sei();                                  // Enable interrupts, next instruction WILL be executed
  sleep_cpu();                            // sleep

  sleep_disable();                        // Clear SE bit
  PCMSK &= ~_BV(PCINT1);                  // Turn off PB1 as interrupt pin, otherwise the interrupt will keep firing. // detachInterrupt
  ADCSRA |= _BV(ADEN);                    // ADC on
  power_timer0_enable();                  // Enable timer0 otherwise delay hangs after waking up
} // sleep

ISR(PCINT0_vect) {
  // Nothing to do here, it will be called when the device is awaken.
}

void loop() {
  digitalWrite(statusLED, LOW);
  
  // HAS to be called first, so that the interrupts are properly set.
  sleep();
  digitalWrite(statusLED, HIGH);
  
  // Awoken from sleep, code continues here.
  // Loop on PB1 and advance the state machine.
  button.attach(buttonPIN);
  
  for(;;) {
    // Advance the state machine.
    // buttonValue = digitalRead(PB1);
    button.read();
    
    switch (state)
    {
      case STATE_SLEEP:
        if (button.low()) {
          state = STATE_FOCUS; // start FOCUS immediatly after a button push
        }
        break;
      case STATE_FOCUS:
        if (button.falling()) { // if button is released and pushed again during FOCUS, a Photo is taken.
          state = STATE_SHUTTER;
        }
        break;
      case STATE_SHUTTER:
        if (button.rising()) { // switch to sleep after button is released (so that is up waiting for next push).
          state = STATE_SLEEP;
        }
        break;
    }

    // Trigger state machine.
    if (state == STATE_FOCUS) {
      digitalWrite(focusPIN, HIGH);
    }
    if (state == STATE_SHUTTER) {
      digitalWrite(shuttPIN, HIGH);
    }
    if (state == STATE_SLEEP) {
      // disable pins
      digitalWrite(focusPIN, LOW);
      digitalWrite(shuttPIN, LOW);

      // TODO -> go to sleep after a certain time, if no state change since last state changed.
      break; // exit loop, go back to sleep();
    }

  }
}
